function RCOOpenWindow()
{
	this.LinkTarget;
	this.LinkDescription;
	this.LinkOpen;
	this.ScrollBars;
	this.FullScreen;
	this.StatusBar;
	this.Height;
	this.Width;
	this.LeftPos;
	this.TopPos;
	this.IsCenter;
	this.IsMaximized;
	this.Resizable;
	this.ToolBar;
	this.IsButton;
	this.VersionNumber;

	this.show = function()
	{
		///UserCodeRegionStart:[show] (do not remove this comment.)

		    // Este UC não tem HTML em tempo de execução
		    //this.setHtml(buffer);  			
		
		
		
		///UserCodeRegionEnd: (do not remove this comment.)
	}
	///UserCodeRegionStart:[User Functions] (do not remove this comment.)

 

	      var outerThis = this;

	      this.OpenWindow = function( IsCenter, vrWidth, vrHeight, IsMaximized, vrLeftPos, vrTopPos, vrFullScreen ){

		    // Se tiver parâmetros passados , configura as propriedades pelos parâmetros em tempo de execução
		    outerThis.IsCenter = IsCenter != null  ? IsCenter  : outerThis.IsCenter;
		    outerThis.Width = vrWidth != null  ? vrWidth  : outerThis.Width;
		    outerThis.Height = vrHeight != null  ? vrHeight  : outerThis.Height;
		    outerThis.IsMaximized = IsMaximized != null  ? IsMaximized  : outerThis.IsMaximized;
		    outerThis.LeftPos = vrLeftPos != null  ? vrLeftPos  : outerThis.LeftPos;
		    outerThis.TopPos = vrTopPos != null  ? vrTopPos  : outerThis.TopPos;
		    outerThis.FullScreen = vrFullScreen != null ? vrFullScreen : outerThis.FullScreen;
		    
		    var popW = outerThis.Width;
		    var popH = outerThis.Height; // Definindo Largura e altura inicial(se a janela não for Maximizada)

		    if (outerThis.IsCenter) {

			  var l = ((window.screen.width - popW) / 2);
			  var t = ((window.screen.height - popH) / 2);

		    }

		    else {

			  var l = outerThis.LeftPos;
			  var t = outerThis.TopPos;

		    }

	            // Abre a janela do Browser conforme as configurações do UC
		    var janela  = window.open(outerThis.LinkOpen, outerThis.LinkUrl, "width=" + popW + ",height=" + outerThis.Height + ",top=" + t + 
		        	" ,left=" + l + ",screenX=" + l + ",screenY=" + t + ",resizable="+ (( outerThis.Resizable)? "1" : "0" ) +
		         	",menubar=" + (( outerThis.ToolBar)? "1" : "0" ) + ",statusbar=" + (( outerThis.StatusBar)? "1" : "0" ) +
		        	",scrollbars=" + (( outerThis.ScrollBars)? "yes" : "no" ) +
		        	",toolbar=0,titlebar=0,directories=0,location=0,fullscreen=" + (( outerThis.FullScreen)? "1" : "0" ));
			
			// Se for configurado para Maximizar a janela do Browser
			if (outerThis.IsMaximized) {
				janela.moveTo( 0, 0 );
				janela.resizeTo( screen.availWidth, screen.availHeight );
			}

			// Põe o foco na janela recem aberta
		    janela.focus();

	      }			


	///UserCodeRegionStart:[User Functions] (do not remove this comment.)

		
	
	
	
	
	///UserCodeRegionEnd: (do not remove this comment.):
}
