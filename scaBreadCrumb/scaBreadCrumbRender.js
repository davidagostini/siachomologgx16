function scaBreadCrumb()
{
	this.Width;
	this.Height;
	this.HomeURL;
	this.Data;
	// Databinding for property Data
	this.SetData = function(data)
	{
		///UserCodeRegionStart:[SetMenuData] (do not remove this comment.)
		this.Data = data;
		///UserCodeRegionEnd: (do not remove this comment.)
	}
	// Databinding for property Data
	this.GetData = function()
	{
		///UserCodeRegionStart:[GetMenuData] (do not remove this comment.)
		return this.Data;		
		///UserCodeRegionEnd: (do not remove this comment.)
	}
	this.show = function() {
	    ///UserCodeRegionStart:[show] (do not remove this comment.)
	    var buffer = new gx.text.stringBuffer();
	    buffer.clear();
	    buffer.append('<ul id="scabreadcrumb">');
	    var homeSrc = gx.util.resourceUrl(gx.basePath + gx.staticDirectory + 'scaBreadCrumb/css/home.gif', true); ;
	    buffer.append('<li><a href="' + this.HomeURL + '"><img src="' + homeSrc + '" alt="Home" class="home" /></a></li>');
	    var i = 0;
	    try {
	        for (i = 0; this.Data[i] != undefined; i++) {
	            var tooltip = (this.Data[i].TooltipText == undefined || this.Data[i].TooltipText == '') ? this.Data[i].Caption : this.Data[i].TooltipText;
	            var target = (this.Data[i].LinkTarget == undefined || this.Data[i].LinkTarget == '') ? '_self' : this.Data[i].LinkTarget;
	            buffer.append('<li><a href="' + this.Data[i].URL + '" title="' + tooltip + '" target="' + target + '">' + this.Data[i].Caption + '</a></li>');
	        }
	        buffer.append('</ul>');
	        this.setHtml(buffer.toString());
        } catch (err) {
	        //Handle errors here
	    }
	    ///UserCodeRegionEnd: (do not remove this comment.)
	}
	///UserCodeRegionStart:[User Functions] (do not remove this comment.)
	///UserCodeRegionEnd: (do not remove this comment.):
}